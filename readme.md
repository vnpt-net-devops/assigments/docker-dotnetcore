# install helm
apt snap install helm --classical
# install traefik
helm repo add traefik https://helm.traefik.io/traefik
helm repo update
helm install traefik traefik/traefik
# testing
kubectl get svc -l app.kubernetes.io/name=traefik
kubectl get po -l app.kubernetes.io/name=traefik
# Access traefik dashboard
kubectl port-forward $(kubectl get pods --selector "app.kubernetes.io/name=traefik" --output=name) 9000:9000


# Build app
docker build . -t myweb-app
# test app outside kubernetes:
# docker run --rm -it -p 80:80 myweb-app

kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
kubectl apply -f ingress.yaml

# kubectl exec --stdin --tty [pod-name] -- /bin/bash

========
# nginx
minikube addons enable ingress